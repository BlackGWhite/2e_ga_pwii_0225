import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsmotosRoutingModule } from './componentsmotos-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ComponentsmotosRoutingModule
  ]
})
export class ComponentsmotosModule { }
